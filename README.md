# LatticeLand 2.0

## Summary
To take a GeoBoard, and explore its concept on a 3-Dimentional level in VR.

![GeoBoard](https://upload.wikimedia.org/wikipedia/commons/thumb/3/3f/Geoplano1.JPG/180px-Geoplano1.JPG)

In the plane, a geo-board allows for the inscription of segments with string or rubber bands.

Lattice land is a spatial extension of a geo-board. Any plane of lattice-land is a 2-d geoboard.

The lattice can be constructed with a variety of geometries. To start with, we have developed two geometries:
 - retangular, where the smallest unit of the geoboard is a cube.
 - equidistant, where the smallest unit of the geoboard is a regular tetrahedron.

## Objectives
In our implementation of lattice land, we will include:
 - [ ] Networked, synchronsous collaboration
 - [ ] Inscription of segments, lines and polygons between points on the lattice
 - [] Hover and select operations, with dynamic feedback for points on the lattice
 - [ ] Automatic recognition of the construction of polygons and polyhedrons, with mesh-rendered surfaces
 - [ ] A prototype badge system for the construction of regular polygons and regular polyhedrons.
 - [ ] Mixed-reality observation with MixCast, with syncronized physical and virtual spaces.
 - [ ] Initial support for SteamVR, the HTC Vive Pro/Pro2 and the Index Controllers


 Eventually we would like to add:
 - [ ] Segment and angle feedback for equality classes

## Implementation

We are using Unity's DOTs to construct an efficient lattice land that can support more systems.

For networking, we currently plan to use Photon's PUN, but may consider open-source alternatives.
Consider the following alternatives:
 - [Mirror](https://github.com/MirrorNetworking/Mirror)
 - [Unity NetCode for ECS](https://github.com/Unity-Technologies/EntityComponentSystemSamples/tree/master/NetcodeSamples)

## Dependencies
In our initial prototype, we aim to minimize external dependencies.

### Unity Version
 - 2022.2.4f1 (As of 2/5/2023)

### Unity Packages & Their First Time Set-Up

Unity.Entites.Graphics
 - Downloading and installing this package automatically gets *Unity.Mathematics and Unity.Entities* as dependencies.
 - To get entities to render properly in scene view, go to: *Edit -> Properties -> Entities*
 - Make *'Scene View Mode' = 'Runtime Data'*

Universal RP
 - Incase it doesn't auto fill the needed settings, go to: *Edit -> Project Settings -> Graphics*
 - Make sure that *'URP-HighFidelity'* is selected as the *'Scriptable Render Pipline Settings'*

OpenXR
 - After downloading and installing, go to: *Edit -> Project Settings -> XR Plug-in Management*
 - Tick *'OpenXR'*. This will trigger an install of dependencies.
 - After, an alert window will pop up, click *'Fix All'* at the top.
 - Go to *OpenXR* that is now a subcategory to XR Plug-in Managment in the side-menu of the *Project Manager*.
 - In the *'Interaction Profiles'* list, add all relevent input device *(As of 2/5/2023: HTC Vive Controller and Valve Index Controller)*

JetBrains Rider Editor (if using Rider)
 - After downloading & installing go to: *Edit -> Preferences -> External Tools*
 - Make *External Script Editor = Rider*

### 3rd Party Assets
 - Photon PUN

## Development
 
### Getting Started

To get started, clone this repository:
`git clone git@gitlab.com:immersive-math/latticeland2`

Then install dependencies (Assets and Packages) in Unity.

### Namespaces
Our development is split across four namespaces:
 - **IMRE.Lattice** The Lattice namespace includes Entities, Components and Systems
 - **IMRE.Lattice.Networking** The Netowrking namespace will include utilities needed to sync avatar and latticesystem states across multiple users.
 - **IMRE.Lattice.XRUI** The XRUI namespace will include ECS and MonoBehaviour based systems for bridging OpenXR input with the Lattice system
 - **IMRE.Lattice.MathUtils** The MathUtils namespace will include static functions for the mathematical backend of LatticeLand.

### DOTS notes
We are currently using DOTS 1.0, in experimental support. Relevant notes will be added below.

## Architecture

### Lattice
The lattice is composed of:

#### LatticeSystem
 - [x] Lattice Instantiation (array of points)
 - [] Logic for Creation of Line Segments (unique line segment for unordered pair)
 - [ ] Logic for Detection of Polygons (possibly async, graph theory representation)
 - [ ] Logic for Detection of Polyhedrons

#### Point ComponentData
 - [x] Vec3Int GridPos
 - [x] Bool Selected
 - [ ] Bool Hover
 - [x] float size (for XRUI) ==> In Aspect
 - [x] float3 color or float hue (for XRUI) ==> In Aspect

#### Point Entity

#### LineSegment ComponentData

#### LineSegment Entity

#### Polygon ComponentData

#### Polygon Entity

#### Polyhedron ComponentData

--

### XRUI
 - OpenXR system
 - Map XRUI --> Point ComponentData


--

### Networking
 - Avatar system
 - Map Networking --> Point ComponentData
 - Check for synched spaces to enable MixCast

--

### MathUtils
TODO: Can these functions operate on ComponentData and not need their own objects in memory?

Minimal:
 - [ ] A graph representation of the lattice, where line segments are relationships between pairs of points, weighted by edge length.
 - [ ] An efficient, parallel function for detection of cycles in a graph.
 - [ ] An efficient function to determine if a set of points are co-planar (i.e., points in a cycle of a graph that may be a polgon).

Extended:
 - [ ] A graph representation of the lattice, where polygons are nodes and adjacent when sides share a segment, with attributes for edge length and angle between faces.
 - [ ] An efficient function to generate triangles and uv normals for a set of coplanar points forming a polygon (including non-convex polygons).
 - [ ] An edge-coloring function by distance equivalence classes.
 - [ ] An edge-coloring function by ditsance and equivalence classes.
