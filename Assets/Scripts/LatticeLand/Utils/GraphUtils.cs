﻿using System;
using System.Collections.Generic;
using System.Linq;
using LatticeLand.Components;
using Unity.Collections;
using Unity.Mathematics;
using UnityEngine;

// cycle detection based on solution at https://stackoverflow.com/questions/12367801/finding-all-cycles-in-undirected-graphs
namespace LatticeLand.Utils
{
    public static class GraphUtils
    {
        public static int2[] GraphOfSegmentNodes(NativeArray<LineSegmentComponent> lineSegments)
        {
            // segments are nodes, points are edges
            List<int2> edges = new List<int2>();

            for (int i = 0; i < lineSegments.Length; i++)
            {
                for (int j = 0; j < lineSegments.Length; j++)
                {
                    // if no matching points, continue
                    if (lineSegments[i].PointAHash == lineSegments[j].PointAHash ||
                        lineSegments[i].PointAHash == lineSegments[j].PointBHash ||
                        lineSegments[i].PointBHash == lineSegments[j].PointAHash ||
                        lineSegments[i].PointBHash == lineSegments[j].PointBHash)
                        edges.Add(new int2(i, j));

                    //if there is a match, add edge to the array.
                }
            }

            return edges.ToArray();
        }

        public static List<int[]> FindAllPolygons(int2[] graph, List<int[]> cycles,
            NativeArray<LineSegmentComponent> segments)
        {
            List<int[]> polygonCycles = new List<int[]>();
            for (int i = 0; i < cycles.Count; i++)
            {
                int[] cycle = cycles[i];
                float3[] pointArray = new float3[cycles[i].Length];
                for (int j = 0; j < cycle.Length; j++)
                {
                    var segment = segments[cycle[j]];
                    var prevSegment = segments[cycle.Length - 1];
                    if (j != 0) prevSegment = segments[j - 1];

                    if (segment.PointAHash == prevSegment.PointAHash || segment.PointAHash == prevSegment.PointBHash)
                    {
                        pointArray[j] =
                            segments[cycle[j]]
                                .PosA; //we get the second position as the start of the next segment (since they are ordered).
                    }
                    else
                    {
                        pointArray[j] =
                            segments[cycle[j]]
                                .PosB; //we get the second position as the start of the next segment (since they are ordered).
                    }
                }

                //if the points are not coplanar, it does not form a polygon.
                if (!CoplanarPoints(pointArray)) continue;
                //otherwise add it to the polygon list
                polygonCycles.Add(cycles[i]);

                PolygonUtils.RenderPolygon(pointArray);
            }

            return polygonCycles;
        }

        public static List<int[]> FindAllRegularPolygons(int2[] graph, List<int[]> cycles,
            NativeArray<LineSegmentComponent> segments)
        {
            List<int[]> regularPolygonCycles = new List<int[]>();
            for (int i = 0; i < cycles.Count; i++)
            {
                float3[] pointArray = new float3[cycles[i].Length];
                for (int j = 0; j < cycles[i].Length; j++)
                {
                    pointArray[j] =
                        segments[j]
                            .PosA; //we get the second position as the start of the next segment (since they are ordered).
                }

                //if the points are not coplanar, it does not form a regular polygon.
                if (!RegularPolygonSegmentCycle(pointArray)) continue;
                //otherwise add it to the regular polygon list
                regularPolygonCycles.Add(cycles[i]);
            }

            return regularPolygonCycles;
        }

        public static List<int[]> FindAllCycles(int2[] graph)
        {
            // TODO: ideally, this is converted to a job system and run in parallel.
            List<int[]> cycles = new List<int[]>();
            for (int i = 0; i < graph.Length; i++)
            {
                int[] candidate = FindNewCycle(graph, graph[i].x, cycles);
                if (candidate.Length > 2)
                {
                    cycles.Add(candidate);
                }

                int[] candidate2 = FindNewCycle(graph, graph[i].y, cycles);
                if (candidate2.Length > 2)
                {
                    cycles.Add(candidate2);
                }
            }

            return cycles;
        }

        private static int[] FindNewCycle(int2[] graph, int startingNode, List<int[]> cycles)
        {
            List<int> path = new List<int>(startingNode);
            int currentNode = startingNode;
            int nextNode = -1;
            int iterations = 0;

            while (iterations < 2)
            {
                for (int i = 0; i < graph.Length; i++)
                {
                    //check to see if the edge matches the current node.
                    if (graph[i].x == currentNode) nextNode = graph[i].y;
                    else if (graph[i].y == currentNode) nextNode = graph[i].x;
                    else continue;

                    // if the candidate node is already in the path, see if it completes the cycle, or skip it.
                    if (VisitedInPath(nextNode, path))
                    {
                        //check if the cycle is complete, if not skip this edge.
                        if (startingNode != nextNode) continue;
                        //otherwise, check if the cycle is in fact a new find!
                        int[] cycle = RotateCycleToSmallest(path.ToArray());
                        if (IsNew(cycles, cycle))
                        {
                            return cycle;
                        }
                    }
                    else
                    {
                        //if not already int he path, add it.
                        path.Add(nextNode);
                        currentNode = nextNode;
                        i = 0; //start looking at the first edge again for new edges;
                    }
                }

                iterations += 1;
            }

            return Array.Empty<int>();
        }

        private static bool VisitedInPath(int node, List<int> path)
        {
            return path.Contains(node);
        }

        private static bool IsNew(List<int[]> cycles, int[] path)
        {
            return !(cycles.Contains(path) || cycles.Contains(InverseCycle(path)));
        }

        private static int[] RotateCycleToSmallest(int[] cycle)
        {
            int idx = cycle.Min();
            if (idx == 0) return cycle;

            // if rotation is necessary, 
            int[] newCycle = new int[cycle.Length];
            int n;

            Array.Copy(cycle, 0, newCycle, 0, cycle.Length);

            while (newCycle[0] != idx)
            {
                n = newCycle[0];
                Array.Copy(newCycle, 1, newCycle, 0, newCycle.Length - 1);
                newCycle[newCycle.Length - 1] = n;
            }

            return newCycle;
        }

        private static int[] InverseCycle(int[] cycle)
        {
            return cycle.Reverse().ToArray();
        }

        public static bool RegularPolygonSegmentCycle(float3[] pointArray)
        {
            return pointArray.Length >= 3 &&
                   CoplanarPoints(pointArray) &&
                   EquilateralSegmentCycle(pointArray) &&
                   EquiangularSegmentCycle(pointArray);
        }

        public static bool EquiangularSegmentCycle(float3[] pointArray)
        {
            int length = pointArray.Length;
            if (length < 3) return true; //there is only one angle

            // find the angle for the first pair
            float angle = InteriorAngle(pointArray[length - 1] - pointArray[0], pointArray[0] - pointArray[1]);
            for (int i = 0; i < length; i++)
            {
                if (!Mathf.Approximately(angle,
                        InteriorAngle(pointArray[(i - 1) % length] - pointArray[i],
                            pointArray[i] - pointArray[(i + 1) % length])))
                    return false;
            }

            return true;
        }

        public static bool EquilateralSegmentCycle(float3[] pointArray)
        {
            //assumes that segments are arranged in the order of the nodes in the point array positions.
            int length = pointArray.Length;
            if (length < 3) return true; // two points are equidistant.

            float distsq = math.distancesq(pointArray[0], pointArray[1]); //find the distance for the first pair
            for (int i = 2; i < length; i++)
            {
                // if one segment fails the equidistant test, return false
                if (!Mathf.Approximately(distsq, math.distance(pointArray[i], pointArray[i - 1]))) return false;
            }

            //if all segments pass, return true
            return true;
        }

        public static bool CoplanarPoints(float3[] pointArray)
        {
            int length = pointArray.Length;
            if (length < 4) return true; //any collection of 0, 1, 2, or 3 points are coplanar.
            // if four or more points, define the plane.
            float3 position = pointArray[0];
            float3 normal = math.normalize(math.cross(pointArray[0] - pointArray[1], pointArray[0] - pointArray[2]));
            for (int i = 3; i < length; i++)
            {
                // if one point fails the coplanar test, return false
                if (!CoplanarPoint(pointArray[i], position, normal)) return false;
            }

            // if all points pass, return true
            return true;
        }

        private static bool CoplanarPoint(float3 point, float3 planePosition, float3 planeNormal)
        {
            return Mathf.Approximately(math.dot(point - planePosition, planeNormal), 0f);
        }

        private static float InteriorAngle(float3 direction1, float3 direction2)
        {
            //assume that the two directions are coplanar and meet at a point
            //by taking the absolute value of the dot product, we find the interior angle.
            return Mathf.Acos(Mathf.Abs(math.dot(math.normalize(direction1), math.normalize(direction2))));
        }
    }
}