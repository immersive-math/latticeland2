﻿using Unity.Mathematics;

namespace LatticeLand.Utils
{
    public static class PointSpawnUtils
    {
        private static float3 LatticePosition(float3x3 basis, int3 coordinates)
        {
            return basis.c0 * coordinates.x + basis.c1 * coordinates.y + basis.c2 * coordinates.z;
        }

        public static float3 LatticePosition(float3x3 basis, int3 coordinates, float spacing, float3 offset)
        {
            basis = spacing * basis;
            return LatticePosition(basis, coordinates) + offset;
        }

        public static float3x3 NormalizeBasis(float3x3 basis)
        {
            basis.c0 = math.normalize(basis.c0);
            basis.c1 = math.normalize(basis.c1);
            basis.c2 = math.normalize(basis.c2);
            return basis;
        }

        public static float3x3 RectangularBasis()
        {
            return new float3x3(
                1f, 0f, 0f,
                0f, 1f, 0f,
                0f, 0f, 1f
            );
        }

        public static float3x3 EquidistantBasis()
        {
            float3x4 tetrahedronVertices = new float3x4(
                1f, 0f, -1f / math.SQRT2,
                -1f, 0f, -1 / math.SQRT2,
                0f, 1f, 1 / math.SQRT2,
                0f, -1f, 1 / math.SQRT2
            );

            float3x3 basis = new float3x3();
            basis.c0 = tetrahedronVertices.c0 - tetrahedronVertices.c2; //shift to c2 at origin
            basis.c1 = tetrahedronVertices.c1 - tetrahedronVertices.c2; //shift to c2 at origin
            basis.c2 = tetrahedronVertices.c3 - tetrahedronVertices.c2; //shift to c2 at origin
            return NormalizeBasis(basis);
        }
    }
}