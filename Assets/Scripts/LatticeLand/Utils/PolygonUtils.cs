﻿namespace LatticeLand.Utils
{
#if false
    public static class PolygonUtils
    {
        public static void RenderPolygon(float3[] positions)
        {
            //if not a polygon skip.
            if (positions.Length < 2) return;

            Material mat = new Material(Shader.Find("Universal Render Pipeline/Lit"));
            RenderParams rp = new RenderParams(mat);
            Mesh mesh = new Mesh();
            mesh.vertices = PolygonVertices(positions);
            mesh.triangles = PolygonTriangles(positions.Length);
            Graphics.RenderMesh(rp, mesh, 0, Matrix4x4.identity);
        }

        private static Vector3[] PolygonVertices(float3[] positions)
        {
            Vector3[] verticies = new Vector3[positions.Length];
            for (int i = 0; i < positions.Length; i++)
            {
                verticies[i] = (Vector3)positions[i];
            }

            return verticies;
        }

        private static int[] PolygonTriangles(int nVertices)
        {
            int[] triangles = new int[(nVertices - 2) * 3];
            for (int i = 0; i < nVertices - 2; i++)
            {
                triangles[i * 3] = 0;
                triangles[i * 3 + 1] = i + 1;
                triangles[i * 3 + 2] = i + 2;
            }

            return triangles;
        }
    }
#endif
}