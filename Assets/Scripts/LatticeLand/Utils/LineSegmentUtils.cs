﻿using Unity.Mathematics;
using UnityEngine;

namespace LatticeLand.Utils
{
    public static class LineSegmentUtils
    {
        public static bool MatchesHash(uint PointAHash, uint PointBHash, uint hash1, uint hash2)
        {
            return ((hash1 == PointAHash &&
                     hash2 == PointBHash) ||
                    (hash1 == PointBHash &&
                     hash2 == PointAHash));
        }

        public static float DistanceSQToSegment(float3 position, float3 pointA, float3 pointB)
        {
            float projection = math.dot(position - pointA, math.normalize(pointB - pointA));
            if (projection <= 0) return math.distancesq(position, pointA); //point A is closest
            if (projection >= math.distance(pointA, pointB)) return math.distance(position, pointB); //pointB is closest
            return math.distancesq(projection * (math.normalize(pointB - pointA)) + pointA, position);
        }

        public static Vector3[] LineSegmentVertices(float lineWidth, float3 posA, float3 posB,
            float3 centerEyePosition, bool hmdPresent = true)
        {
            if (!hmdPresent)
            {
                //if no hmd, render line segments from above.
                centerEyePosition = new float3(0, 10, 0);
            }

            float3 direction = math.normalize(posB - posA);
            float3 directionToEye = math.normalize(((posA + posB) / (2f)) - centerEyePosition);
            float3 cross = lineWidth * math.normalize(math.cross(direction, directionToEye));

            Vector3[] res = new Vector3[4];
            res[0] = posA + cross;
            res[1] = posA - cross;
            res[2] = posB + cross;
            res[3] = posB - cross;

            return res;
        }

        public static int[] LineSegmentTriangles()
        {
            int[] res = new int[6];
            res[0] = 0;
            res[1] = 1;
            res[2] = 3;
            res[3] = 0;
            res[4] = 3;
            res[5] = 2;

            return res;
        }
    }
}