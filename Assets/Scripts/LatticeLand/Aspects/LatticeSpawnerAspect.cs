using LatticeLand.Components;
using Unity.Entities;
using Unity.Mathematics;
using Unity.Transforms;

namespace LatticeLand.Aspects
{
    public readonly partial struct LatticeSpawnerAspect : IAspect
    {
        public readonly Entity Entity;

        private readonly TransformAspect _transformAspect;

        private readonly RefRO<InitialPointSpawnerComponent> _latticeGridProperties;
        private readonly RefRO<InitialPointSpawnerBasisComponent> _latticeBasis;

        public int3 dimensionsOfLatticeGrid => _latticeGridProperties.ValueRO.GridDimensions;
        public float gapOfPoints => _latticeGridProperties.ValueRO.GapOfPoints;
        public Entity gridPointPrefab => _latticeGridProperties.ValueRO.GridPointPrefab;
        public Entity lineSegmentPrefab => _latticeGridProperties.ValueRO.LineSegmentPrefab;
        public float scaleOfPoints => _latticeGridProperties.ValueRO.ScaleOfPoints;

        public float3x3 basis => _latticeBasis.ValueRO.basis;

        public float3 offsetPosition => _transformAspect.LocalPosition;
    }
}