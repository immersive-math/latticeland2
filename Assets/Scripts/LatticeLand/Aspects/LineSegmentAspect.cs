﻿using LatticeLand.Components;
using LatticeLand.Utils;
using Unity.Entities;
using Unity.Mathematics;
using Unity.Rendering;
using Unity.Transforms;
using UnityEngine;

namespace LatticeLand.Aspects
{
    public readonly partial struct LineSegmentAspect : IAspect
    {
        #region Refference Variables

        // Reference Variables
        private readonly TransformAspect m_TransformAspect;
        private readonly RefRW<LineSegmentComponent> m_LineSegmentProperties;
        private readonly RefRW<LineSegmentAestheticComponent> m_LineSegmentAstheticProperties;
        private readonly RefRW<URPMaterialPropertyBaseColor> m_MaterialColor;

        #endregion

        #region Materials

        // Material Colors
        private float4 idleStateColor => m_LineSegmentAstheticProperties.ValueRO.IdleStateColor;
        private float4 hoverStateColor => m_LineSegmentAstheticProperties.ValueRO.HoverStateColor;
        private float4 selectedStateColor => m_LineSegmentAstheticProperties.ValueRO.SelectedStateColor;

        #endregion

        #region Scales

        // Scales
        private float idleStateScale => m_LineSegmentAstheticProperties.ValueRO.IdleStateScale;
        private float hoverStateScale => m_LineSegmentAstheticProperties.ValueRO.HoverStateScale;
        private float selectedStateScale => m_LineSegmentAstheticProperties.ValueRO.SelectedStateScale;

        #endregion Scales

        public int3 pointAGridCoordinates => m_LineSegmentProperties.ValueRO.PointAGridCoordinates;
        public int3 pointBGridCoordinates => m_LineSegmentProperties.ValueRO.PointBGridCoordinates;
        public float3 posA => m_LineSegmentProperties.ValueRO.PosA;
        public float3 posB => m_LineSegmentProperties.ValueRO.PosB;

        public float lineWidth => m_LineSegmentAstheticProperties.ValueRO.LineWidth;

        public bool isSelected => m_LineSegmentProperties.ValueRW.IsSelected;

        public bool SetHoverByProximity(float3 controllerPos, float radiusSq, bool selectMode)
        {
            if (!(LineSegmentUtils.DistanceSQToSegment(controllerPos, posA, posB) < radiusSq))
                return false;

            if (selectMode)
            {
                SetSelectedState();
            }
            else
            {
                SetHoverState();
            }

            return selectMode;
        }

        private void SetHoverState()
        {
            m_LineSegmentAstheticProperties.ValueRW.LineWidth = hoverStateScale;
            m_MaterialColor.ValueRW.Value = hoverStateColor;
            m_LineSegmentProperties.ValueRW.IsSelected = false;
        }

        private void SetSelectedState()
        {
            m_LineSegmentAstheticProperties.ValueRW.LineWidth = selectedStateScale;
            m_MaterialColor.ValueRW.Value = selectedStateColor;
            m_LineSegmentProperties.ValueRW.IsSelected = true;
        }

        public void SetIdleState()
        {
            m_LineSegmentAstheticProperties.ValueRW.LineWidth = idleStateScale;
            m_MaterialColor.ValueRW.Value = idleStateColor;
        }

        public void RenderSegment(float3 cameraPos, bool isTracked)
        {
            // TODO: the gereration of materials is extremely ineficient
            Material mat = new Material(Shader.Find("Universal Render Pipeline/Lit"));
            mat.color = _color(m_MaterialColor.ValueRO.Value);
            RenderParams rp = new RenderParams(mat);
            Mesh mesh = new Mesh();
            mesh.vertices = LineSegmentUtils.LineSegmentVertices(lineWidth, posA, posB, cameraPos, isTracked);
            mesh.triangles = LineSegmentUtils.LineSegmentTriangles();
            Graphics.RenderMesh(rp, mesh, 0, Matrix4x4.identity);
        }

        private Color _color(float4 color)
        {
            return new Color(color.x, color.y, color.z, color.w);
        }
    }
}