using LatticeLand.Components;
using Unity.Entities;
using Unity.Mathematics;
using Unity.Rendering;
using Unity.Transforms;

namespace LatticeLand.Aspects
{
    public readonly partial struct PointAspect : IAspect
    {
        #region Refference Variables

        // Reference Variables
        public readonly TransformAspect m_TransformAspect;
        private readonly RefRW<PointComponent> m_GridPointProperties;
        private readonly RefRW<PointAestheticsComponent> m_GridPointAesthetics;
        private readonly RefRW<URPMaterialPropertyBaseColor> m_MaterialColor;

        #endregion

        #region Materials

        // Material Colors
        private float4 idleStateColor => m_GridPointAesthetics.ValueRO.IdleStateColor;
        private float4 hoverStateColor => m_GridPointAesthetics.ValueRO.HoverStateColor;
        private float4 selectedStateColor => m_GridPointAesthetics.ValueRO.SelectedStateColor;

        #endregion

        #region Scales

        // Scales
        private float idleStateScale => m_GridPointAesthetics.ValueRW.IdleStateScale;
        private float hoverStateScale => m_GridPointAesthetics.ValueRW.HoverStateScale;
        private float selectedStateScale => m_GridPointAesthetics.ValueRW.SelectedStateScale;

        #endregion Scales

        #region Logic Variables

        // Logic Variables
        public int3 gridPointCoordinates => m_GridPointProperties.ValueRO.GridPointCoordinates;
        public uint gridCoordHash => m_GridPointProperties.ValueRO.CoordinatesHash;
        public bool isSelected => m_GridPointProperties.ValueRW.IsSelected;

        #endregion

        #region Functions

        // Functions
        private void SetHoverState()
        {
            m_TransformAspect.LocalScale = hoverStateScale;
            m_MaterialColor.ValueRW.Value = hoverStateColor;
        }

        private void SetSelectedState()
        {
            m_TransformAspect.LocalScale = selectedStateScale;
            m_MaterialColor.ValueRW.Value = selectedStateColor;
            m_GridPointProperties.ValueRW.IsSelected = true;
        }

        public void SetIdleState()
        {
            m_TransformAspect.LocalScale = idleStateScale;
            m_MaterialColor.ValueRW.Value = idleStateColor;
            m_GridPointProperties.ValueRW.IsSelected = false;
        }

        private float WorldSpaceDistanceSqTo(float3 worldSpacePosition)
        {
            return math.distancesq(worldSpacePosition, m_TransformAspect.WorldPosition);
        }

        public bool SetHoverByProximity(float3 worldSpacePosition, float radiusSq, bool selectMode)
        {
            if (!(WorldSpaceDistanceSqTo(worldSpacePosition) < radiusSq))
                return false;

            if (selectMode)
            {
                SetSelectedState();
            }
            else
            {
                SetHoverState();
            }

            return selectMode;
        }

        #endregion
    }
}