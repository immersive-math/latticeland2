﻿using LatticeLand.Utils;
using Unity.Entities;
using Unity.Mathematics;
using UnityEngine;

namespace LatticeLand.Components
{
    public struct InitialPointSpawnerBasisComponent : IComponentData
    {
        public float3x3 basis;
    }

    public class InitialPointSpawnerBasis : MonoBehaviour
    {
        public float3x3 basis;
        public bool overrideRectangular;
        public bool overrideEquilateral;
    }

    public class InitialPointSpawnerBasisBaker : Baker<InitialPointSpawnerBasis>
    {
        public override void Bake(InitialPointSpawnerBasis authoring)
        {
            if (authoring.overrideRectangular)
            {
                AddComponent(new InitialPointSpawnerBasisComponent
                {
                    basis = PointSpawnUtils.RectangularBasis()
                });
            }
            else if (authoring.overrideEquilateral)
            {
                AddComponent(new InitialPointSpawnerBasisComponent
                {
                    basis = PointSpawnUtils.EquidistantBasis()
                });
            }
            else
            {
                AddComponent(new InitialPointSpawnerBasisComponent
                {
                    basis = PointSpawnUtils.NormalizeBasis(authoring.basis)
                });
            }
        }
    }
}