using Unity.Entities;
using Unity.Mathematics;
using UnityEngine;

namespace LatticeLand.Components
{
    public struct InitialPointSpawnerComponent : IComponentData
    {
        public int3 GridDimensions;
        public Entity GridPointPrefab;
        public Entity LineSegmentPrefab;
        public float GapOfPoints;
        public float ScaleOfPoints;
    }

    public class InitialPointSpawner : MonoBehaviour
    {
        public int3 gridDimensions;

        public float gapOfPoints;
        public float scaleOfPoints;
        public GameObject gridPointPrefab;
        public GameObject lineSegmentPrefab;
    }

    public class InitialPointSpawnerBaker : Baker<InitialPointSpawner>
    {
        public override void Bake(InitialPointSpawner authoring)
        {
            AddComponent(new InitialPointSpawnerComponent
            {
                GridDimensions = authoring.gridDimensions,
                GapOfPoints = authoring.gapOfPoints,
                ScaleOfPoints = authoring.scaleOfPoints,
                GridPointPrefab = GetEntity(authoring.gridPointPrefab),
                LineSegmentPrefab = GetEntity(authoring.lineSegmentPrefab)
            });
        }
    }
}