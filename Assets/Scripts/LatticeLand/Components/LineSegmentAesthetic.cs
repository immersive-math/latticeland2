﻿using Unity.Entities;
using Unity.Mathematics;
using UnityEngine;

namespace LatticeLand.Components
{
    public struct LineSegmentAestheticComponent : IComponentData
    {
        public float LineWidth;

        // Material Colors
        public float4 IdleStateColor;

        public float4 HoverStateColor;
        public float4 SelectedStateColor;

        // Scales
        public float IdleStateScale;
        public float HoverStateScale;
        public float SelectedStateScale;
    }

    public class LineSegmentAesthetic : MonoBehaviour
    {
        public float lineWidth;
        [Header("Material Colors")] public float4 idleStateColor;
        public float4 hoverStateColor;
        public float4 selectedStateColor;

        // Scales
        [Header("Line Segment Scales")] public float idleStateScale;
        public float hoverStateScale;
        public float selectedStateScale;
    }

    public class LineSegmentAestheticsBaker : Baker<LineSegmentAesthetic>
    {
        public override void Bake(LineSegmentAesthetic authoring)
        {
            AddComponent(new LineSegmentAestheticComponent()
            {
                LineWidth = authoring.lineWidth,
                IdleStateColor = authoring.idleStateColor,
                HoverStateColor = authoring.hoverStateColor,
                SelectedStateColor = authoring.selectedStateColor,
                IdleStateScale = authoring.idleStateScale,
                HoverStateScale = authoring.hoverStateScale,
                SelectedStateScale = authoring.selectedStateScale
            });
        }
    }
}