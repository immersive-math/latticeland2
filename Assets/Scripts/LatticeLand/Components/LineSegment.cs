using Unity.Entities;
using Unity.Mathematics;
using UnityEngine;

namespace LatticeLand.Components
{
    public struct LineSegmentComponent : IComponentData
    {
        public int3 PointAGridCoordinates;
        public int3 PointBGridCoordinates;

        public uint PointAHash;
        public uint PointBHash;

        public float3 PosA;
        public float3 PosB;

        public bool IsSelected;
    }

    public class LineSegment : MonoBehaviour
    {
        public int3 pointAGridCoordinates;
        public int3 pointBGridCoordinates;

        public float3 posA;
        public float3 posB;

        public bool isSelected;
    }

    public class LineSegmentBaker : Baker<LineSegment>
    {
        public override void Bake(LineSegment authoring)
        {
            AddComponent(new LineSegmentComponent
            {
                PointAGridCoordinates = authoring.pointAGridCoordinates,
                PointBGridCoordinates = authoring.pointBGridCoordinates,
                PosA = authoring.posA,
                PosB = authoring.posB,
                IsSelected = authoring.isSelected
            });
        }
    }
}