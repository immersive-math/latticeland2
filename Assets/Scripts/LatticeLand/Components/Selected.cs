﻿using Unity.Entities;
using UnityEngine;

namespace LatticeLand.Components
{
    public struct Selected : IComponentData
    {
        public float selectionTime;
    }

    public class SelectedMono : MonoBehaviour
    {
        // Logic variables
        [Header("Logic Variables")] public float selectionTime;
    }

    public class SelectedBaker : Baker<SelectedMono>
    {
        public override void Bake(SelectedMono authoring)
        {
            AddComponent(new Selected
            {
                selectionTime = authoring.selectionTime
            });
        }
    }
}