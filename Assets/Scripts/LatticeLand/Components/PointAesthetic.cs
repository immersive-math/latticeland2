﻿using Unity.Entities;
using Unity.Mathematics;
using UnityEngine;

namespace LatticeLand.Components
{
    public struct PointAestheticsComponent : IComponentData
    {
        /// <summary>
        /// Author: Matthew Patterson
        /// 
        /// This component stores the materials and scales
        /// for the three states a Grid Point can be:
        /// 1. Idle
        /// 2. Hover
        /// 3. Selected
        /// 
        /// </summary>

        // Material Colors
        public float4 IdleStateColor;

        public float4 HoverStateColor;
        public float4 SelectedStateColor;

        // Scales
        public float IdleStateScale;
        public float HoverStateScale;
        public float SelectedStateScale;
    }

    public class PointAesthetic : MonoBehaviour
    {
        // Material Colors
        [Header("Material Colors")] public float4 idleStateColor;
        public float4 hoverStateColor;
        public float4 selectedStateColor;

        // Scales
        [Header("Point Scales")] public float idleStateScale;
        public float hoverStateScale;
        public float selectedStateScale;
    }

    public class PointAestheticsBaker : Baker<PointAesthetic>
    {
        public override void Bake(PointAesthetic authoring)
        {
            AddComponent(new PointAestheticsComponent
            {
                IdleStateColor = authoring.idleStateColor,
                HoverStateColor = authoring.hoverStateColor,
                SelectedStateColor = authoring.selectedStateColor,
                IdleStateScale = authoring.idleStateScale,
                HoverStateScale = authoring.hoverStateScale,
                SelectedStateScale = authoring.selectedStateScale
            });
        }
    }
}