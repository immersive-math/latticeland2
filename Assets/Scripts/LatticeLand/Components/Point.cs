using Unity.Entities;
using Unity.Mathematics;
using UnityEngine;

namespace LatticeLand.Components
{
    public struct PointComponent : IComponentData
    {
        public int3 GridPointCoordinates;
        public uint CoordinatesHash;

        // Logic Variables
        public bool IsSelected;
    }

    public class Point : MonoBehaviour
    {
        // Logic variables
        [Header("Logic Variables")] public int3 gridPointCoordinates;
        public bool isSelected;
    }

    public class PointBaker : Baker<Point>
    {
        public override void Bake(Point authoring)
        {
            AddComponent(new PointComponent
            {
                GridPointCoordinates = authoring.gridPointCoordinates,
                IsSelected = authoring.isSelected
            });
        }
    }
}