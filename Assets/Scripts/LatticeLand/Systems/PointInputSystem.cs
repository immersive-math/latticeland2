using InputOpenXR.Aspects;
using LatticeLand.Aspects;
using LatticeLand.Components;
using Unity.Burst;
using Unity.Entities;
using UnityEngine;

namespace LatticeLand.Systems
{
    [BurstCompile]
    [UpdateInGroup(typeof(SimulationSystemGroup))]
    public partial struct PointInputSystem : ISystem
    {
        /// <summary>
        /// Author: Matthew Patterson
        ///
        /// Behavior:
        ///
        /// </summary>
        [BurstCompile]
        public void OnCreate(ref SystemState state)
        {
        }

        [BurstCompile]
        public void OnDestroy(ref SystemState state)
        {
        }

        [BurstCompile]
        public void OnUpdate(ref SystemState state)
        {
            var ecb = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>()
                .CreateCommandBuffer(state.WorldUnmanaged);
            // hover and select with simulated controller
            foreach (var (gridPoint, pointEntity) in SystemAPI.Query<PointAspect>().WithEntityAccess())
            {
                var selected = false;
                gridPoint.SetIdleState();
                foreach (var controller in SystemAPI.Query<XRControllerAspect>())
                {
                    selected = selected ||
                               gridPoint.SetHoverByProximity(controller.pos, controller.radius, controller.buttonDown);
                }

                //if not selected, remove selection component if it exists
                if (!selected)
                {
                    ecb.RemoveComponent<Selected>(pointEntity);
                    continue;
                }

                //if selected, add hover 
                ecb.AddComponent(pointEntity, new Selected
                {
                    selectionTime = Time.time
                });
            }
        }
    }
}