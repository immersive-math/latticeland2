﻿using System.Collections.Generic;
using LatticeLand.Components;
using LatticeLand.Utils;
using Unity.Burst;
using Unity.Collections;
using Unity.Entities;
using Unity.Mathematics;

namespace LatticeLand.Systems
{
    [BurstCompile]
    [UpdateInGroup(typeof(PresentationSystemGroup))]
    public partial struct PolygonDetectionSystem : ISystem
    {
        private EntityQuery _lineSegments;

        [BurstCompile]
        public void OnCreate(ref SystemState state)
        {
            var lineSegmentsBuilder = new EntityQueryBuilder(Allocator.Temp);
            lineSegmentsBuilder.WithAll<LineSegmentComponent>();
            _lineSegments = state.GetEntityQuery(lineSegmentsBuilder);
        }

        [BurstCompile]
        public void OnDestroy(ref SystemState state)
        {
        }

        [BurstCompile]
        public void OnUpdate(ref SystemState state)
        {
            // TODO: efficiently find all cycles of line segments --> polygons.
            var lineSegmentProperties = _lineSegments.ToComponentDataArray<LineSegmentComponent>(Allocator.Temp);

            // make a graph
            int2[] graph = GraphUtils.GraphOfSegmentNodes(lineSegmentProperties);

            //find cycles
            List<int[]> cycles = GraphUtils.FindAllCycles(graph);

            //identify polygons
            List<int[]> polygons = GraphUtils.FindAllPolygons(graph, cycles, lineSegmentProperties);

            //TODO: make componentData for polygons, render using a new system.
        }
    }
}