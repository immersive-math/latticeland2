using LatticeLand.Aspects;
using LatticeLand.Components;
using LatticeLand.Utils;
using Unity.Burst;
using Unity.Collections;
using Unity.Entities;
using Unity.Mathematics;
using Unity.Transforms;

namespace LatticeLand.Systems
{
    [BurstCompile]
    [UpdateInGroup(typeof(InitializationSystemGroup))]
    public partial struct InitialPointSpawnerSystem : ISystem
    {
        [BurstCompile]
        public void OnCreate(ref SystemState state)
        {
            state.RequireForUpdate<InitialPointSpawnerComponent>();
        }

        [BurstCompile]
        public void OnDestroy(ref SystemState state)
        {
        }

        [BurstCompile]
        public void OnUpdate(ref SystemState state)
        {
            // This forces this to run once on launch.
            state.Enabled = false;

            var latticeGridEntity = SystemAPI.GetSingletonEntity<InitialPointSpawnerComponent>();
            var latticeGrid = SystemAPI.GetAspectRW<LatticeSpawnerAspect>(latticeGridEntity);

            var entityCommandBuffer = new EntityCommandBuffer(Allocator.Temp);
            for (int x = 0; x < latticeGrid.dimensionsOfLatticeGrid.x; x++)
            {
                for (int y = 0; y < latticeGrid.dimensionsOfLatticeGrid.y; y++)
                {
                    for (int z = 0; z < latticeGrid.dimensionsOfLatticeGrid.z; z++)
                    {
                        int3 coordinates = new int3(x, y, z);

                        var newGridPoint = entityCommandBuffer.Instantiate(latticeGrid.gridPointPrefab);

                        entityCommandBuffer.SetComponent(newGridPoint,
                            new LocalTransform
                            {
                                _Position = PointSpawnUtils.LatticePosition(latticeGrid.basis,
                                    coordinates, latticeGrid.gapOfPoints, latticeGrid.offsetPosition),
                                _Scale = latticeGrid.scaleOfPoints,
                                _Rotation = quaternion.identity
                            });

                        entityCommandBuffer.SetComponent(newGridPoint,
                            new PointComponent
                            {
                                GridPointCoordinates = coordinates,
                                CoordinatesHash = math.hash(coordinates)
                            });
                    }
                }
            }

            entityCommandBuffer.Playback(state.EntityManager);
        }
    }
}