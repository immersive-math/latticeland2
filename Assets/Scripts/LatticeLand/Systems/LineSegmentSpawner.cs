﻿using LatticeLand.Aspects;
using LatticeLand.Components;
using LatticeLand.Utils;
using Unity.Burst;
using Unity.Collections;
using Unity.Entities;
using Unity.Transforms;

namespace LatticeLand.Systems
{
    [BurstCompile]
    [UpdateInGroup(typeof(VariableRateSimulationSystemGroup))]
    public partial struct LineSegmentSpawner : ISystem
    {
        private EntityQuery _selectedGridPoints;
        private EntityQuery _lineSegments;

        [BurstCompile]
        public void OnCreate(ref SystemState state)
        {
            var selectedGridPointsBuilder = new EntityQueryBuilder(Allocator.Temp);
            selectedGridPointsBuilder.WithAll<PointComponent, WorldTransform, Selected>();
            _selectedGridPoints = state.GetEntityQuery(selectedGridPointsBuilder);

            var lineSegmentsBuilder = new EntityQueryBuilder(Allocator.Temp);
            lineSegmentsBuilder.WithAll<LineSegmentComponent>();
            _lineSegments = state.GetEntityQuery(lineSegmentsBuilder);
        }

        [BurstCompile]
        public void OnDestroy(ref SystemState state)
        {
        }

        [BurstCompile]
        public void OnUpdate(ref SystemState state)
        {
            // instantiate line segments

            var ecb = SystemAPI.GetSingleton<EndFixedStepSimulationEntityCommandBufferSystem.Singleton>()
                .CreateCommandBuffer(state.WorldUnmanaged);
            var latticeGridEntity = SystemAPI.GetSingletonEntity<InitialPointSpawnerComponent>();
            var latticeGrid = SystemAPI.GetAspectRW<LatticeSpawnerAspect>(latticeGridEntity);

            var gridPointProperties = _selectedGridPoints.ToComponentDataArray<PointComponent>(Allocator.Temp);
            var gridPointTransform = _selectedGridPoints.ToComponentDataArray<WorldTransform>(Allocator.Temp);
            var lineSegmentProperties = _lineSegments.ToComponentDataArray<LineSegmentComponent>(Allocator.Temp);

            for (int i = 0; i < gridPointProperties.Length; i++)
            {
                var gridPointA = gridPointProperties[i];

                for (int j = i; j < gridPointProperties.Length; j++)
                {
                    var gridPointB = gridPointProperties[j];
                    if (!gridPointB.IsSelected ||
                        (gridPointA.CoordinatesHash == gridPointB.CoordinatesHash)) continue;

                    // assume that the new line segment is unique 
                    bool unique = true;
                    foreach (var lineSegment in lineSegmentProperties)
                    {
                        // if there is no match, continue
                        if (!LineSegmentUtils.MatchesHash(lineSegment.PointAHash, lineSegment.PointBHash,
                                gridPointA.CoordinatesHash, gridPointB.CoordinatesHash))
                            continue;
                        // if its not unique, break
                        unique = false;
                        break;
                    }

                    // if not unique continue to next point pair
                    if (!unique) continue;
                    // if it is unique generate a new line segment
                    var newLineSegment = ecb.Instantiate(latticeGrid.lineSegmentPrefab);
                    ecb.SetComponent(newLineSegment, new LineSegmentComponent
                    {
                        PointAGridCoordinates = gridPointA.GridPointCoordinates,
                        PointBGridCoordinates = gridPointB.GridPointCoordinates,
                        PointAHash = gridPointA.CoordinatesHash,
                        PointBHash = gridPointB.CoordinatesHash,
                        PosA = gridPointTransform[i].Position,
                        PosB = gridPointTransform[j].Position
                    });
                }
            }
        }
    }
}