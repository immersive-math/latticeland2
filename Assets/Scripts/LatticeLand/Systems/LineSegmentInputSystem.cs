﻿using InputOpenXR.Aspects;
using LatticeLand.Aspects;
using LatticeLand.Components;
using Unity.Burst;
using Unity.Entities;
using UnityEngine;

namespace LatticeLand.Systems
{
    public struct LineSegmentInputSystem
    {
        /// <summary>
        /// Author: Matthew Patterson
        ///
        /// Behavior:
        ///
        /// </summary>
        [BurstCompile]
        public void OnCreate(ref SystemState state)
        {
        }

        [BurstCompile]
        public void OnDestroy(ref SystemState state)
        {
        }

        [BurstCompile]
        public void OnUpdate(ref SystemState state)
        {
            var ecb = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>()
                .CreateCommandBuffer(state.WorldUnmanaged);
            // hover and select with simulated controller
            foreach (var (lineSegment, entity) in SystemAPI.Query<LineSegmentAspect>().WithEntityAccess())
            {
                var selected = false;
                lineSegment.SetIdleState();
                foreach (var controller in SystemAPI.Query<XRControllerAspect>())
                {
                    selected = selected ||
                               lineSegment.SetHoverByProximity(controller.pos, controller.radius,
                                   controller.buttonDown);
                }

                //if not selected, remove selection component if it exists
                if (!selected)
                {
                    ecb.RemoveComponent<Selected>(entity);
                    continue;
                }

                //if selected, add hover 
                ecb.AddComponent(entity, new Selected
                {
                    selectionTime = Time.time
                });
            }
        }
    }
}