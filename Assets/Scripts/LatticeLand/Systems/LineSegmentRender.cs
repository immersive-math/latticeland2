﻿using InputOpenXR.Components;
using LatticeLand.Aspects;
using Unity.Burst;
using Unity.Entities;

namespace LatticeLand.Systems
{
    [BurstCompile]
    [UpdateInGroup(typeof(PresentationSystemGroup))]
    public partial struct LineSegmentRender : ISystem
    {
        [BurstCompile]
        public void OnCreate(ref SystemState state)
        {
        }

        [BurstCompile]
        public void OnDestroy(ref SystemState state)
        {
        }

        [BurstCompile]
        public void OnUpdate(ref SystemState state)
        {
            HeadMountedDisplayComponent camera = SystemAPI.GetSingleton<HeadMountedDisplayComponent>();
            foreach (var lineSegment in SystemAPI.Query<LineSegmentAspect>())
            {
                lineSegment.RenderSegment(camera.centerEyePosition, camera.isTracked);
            }
        }
    }
}