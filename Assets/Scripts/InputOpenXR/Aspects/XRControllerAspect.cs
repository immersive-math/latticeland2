using System.Collections.Generic;
using InputOpenXR.Components;
using Unity.Entities;
using Unity.Mathematics;
using Unity.Transforms;
using UnityEngine;
using UnityEngine.XR;

namespace InputOpenXR.Aspects
{
    public readonly partial struct XRControllerAspect : IAspect
    {
        #region Refference Variables

        // Reference Variables
        public readonly Entity Entity;
        public readonly TransformAspect m_TransformAspect;
        private readonly RefRW<ControllerComponent> m_XRControllerProperties;

        public float3 pos => m_TransformAspect.WorldPosition;
        public bool isTracked => m_XRControllerProperties.ValueRO.isTracked;
        public float radius => m_XRControllerProperties.ValueRO.radius;
        public bool buttonDown => m_XRControllerProperties.ValueRO.triggerPressed;

        #endregion

        #region Functions: Get Blittable Data

        // These functions are used to return the blittable version of the devices XRNodes

        public void UpdateOpenXRPosition()
        {
            var devices = new List<InputDevice>();
            if (m_XRControllerProperties.ValueRO.isLeft)
            {
                InputDevices.GetDevicesAtXRNode(XRNode.LeftHand, devices);
            }
            else
            {
                InputDevices.GetDevicesAtXRNode(XRNode.RightHand, devices);
            }

            if (devices.Count != 1) return;
            InputDevice device = devices[0];
            bool tracked = false;
            Vector3 position = default;
            Quaternion rotation = default;
            if (device.TryGetFeatureValue(CommonUsages.devicePosition, out position))
            {
                m_TransformAspect.LocalPosition = (float3)position;
            }

            if (device.TryGetFeatureValue(CommonUsages.deviceRotation, out rotation))
            {
                m_TransformAspect.LocalRotation = (quaternion)rotation;
            }

            if (device.TryGetFeatureValue(CommonUsages.isTracked, out tracked))
            {
                m_XRControllerProperties.ValueRW.isTracked = tracked;
            }
        }

        public void UpdateOpenXRInput()
        {
            var devices = new List<InputDevice>();
            if (m_XRControllerProperties.ValueRO.isLeft)
            {
                InputDevices.GetDevicesAtXRNode(XRNode.LeftHand, devices);
            }
            else
            {
                InputDevices.GetDevicesAtXRNode(XRNode.RightHand, devices);
            }

            if (devices.Count != 1) return;
            InputDevice device = devices[0];
            bool trigger = false;
            if (device.TryGetFeatureValue(CommonUsages.triggerButton, out trigger))
            {
                m_XRControllerProperties.ValueRW.triggerPressed = trigger;
            }

            float triggerValue = 0;
            if (device.TryGetFeatureValue(CommonUsages.trigger, out triggerValue))
            {
                m_XRControllerProperties.ValueRW.triggerValue = triggerValue;
            }

            bool grip = false;
            if (device.TryGetFeatureValue(CommonUsages.gripButton, out grip))
            {
                m_XRControllerProperties.ValueRW.squeezeClick = grip;
            }

            float gripValue = 0;
            if (device.TryGetFeatureValue(CommonUsages.grip, out gripValue))
            {
                m_XRControllerProperties.ValueRW.squeezeValue = gripValue;
            }

            bool trackpadTouch = false;
            if (device.TryGetFeatureValue(CommonUsages.primary2DAxisTouch, out trackpadTouch))
            {
                m_XRControllerProperties.ValueRW.trackpadClick = trackpadTouch;
            }

            bool trackpadClick = false;
            if (device.TryGetFeatureValue(CommonUsages.primary2DAxisClick, out trackpadClick))
            {
                m_XRControllerProperties.ValueRW.trackpadClick = trackpadClick;
            }

            Vector2 trackpad = default;
            if (device.TryGetFeatureValue(CommonUsages.primary2DAxis, out trackpad))
            {
                m_XRControllerProperties.ValueRW.trackpad = (float2)trackpad;
            }

            bool primaryClick = false;
            if (device.TryGetFeatureValue(CommonUsages.primaryButton, out primaryClick))
            {
                m_XRControllerProperties.ValueRW.primaryClick = primaryClick;
            }

            bool secondaryClick = false;
            if (device.TryGetFeatureValue(CommonUsages.secondaryButton, out secondaryClick))
            {
                m_XRControllerProperties.ValueRW.secondaryClick = secondaryClick;
            }

            bool menuClick = false;
            if (device.TryGetFeatureValue(CommonUsages.menuButton, out menuClick))
            {
                m_XRControllerProperties.ValueRW.menuClick = menuClick;
            }
        }

        #endregion
    }
}