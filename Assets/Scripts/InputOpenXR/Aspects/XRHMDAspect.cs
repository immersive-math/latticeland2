using System.Collections.Generic;
using InputOpenXR.Components;
using Unity.Entities;
using Unity.Mathematics;
using Unity.Transforms;
using UnityEngine;
using UnityEngine.XR;

namespace InputOpenXR.Aspects
{
    public readonly partial struct XRHMDAspect : IAspect
    {
        #region Refference Variables

        // Reference Variables
        public readonly Entity Entity;
        public readonly TransformAspect m_TransformAspect;
        private readonly RefRW<HeadMountedDisplayComponent> m_XRHMDProperties;

        public float3 pos => m_TransformAspect.WorldPosition;
        public bool isTracked => m_XRHMDProperties.ValueRO.isTracked;
        public float3 centerEye => m_XRHMDProperties.ValueRW.centerEyePosition;

        #endregion

        #region Functions: Get Blittable Data

        // These functions are used to return the blittable version of the devices XRNodes

        public void UpdateOpenXRPosition()
        {
            var devices = new List<InputDevice>();
            InputDevices.GetDevicesAtXRNode(XRNode.Head, devices);

            if (devices.Count != 1) return;
            InputDevice device = devices[0];
            bool tracked = false;
            Vector3 position = default;
            Quaternion rotation = default;
            if (device.TryGetFeatureValue(CommonUsages.devicePosition, out position))
            {
                m_TransformAspect.LocalPosition = (float3)position;
            }

            if (device.TryGetFeatureValue(CommonUsages.deviceRotation, out rotation))
            {
                m_TransformAspect.LocalRotation = (quaternion)rotation;
            }

            if (device.TryGetFeatureValue(CommonUsages.isTracked, out tracked))
            {
                m_XRHMDProperties.ValueRW.isTracked = tracked;
            }
        }

        public void UpdateOpenXRcenterEye()
        {
            var devices = new List<InputDevice>();
            InputDevices.GetDevicesAtXRNode(XRNode.CenterEye, devices);

            if (devices.Count != 1) return;
            InputDevice device = devices[0];
            Vector3 position = default;

            if (device.TryGetFeatureValue(CommonUsages.centerEyePosition, out position))
            {
                m_XRHMDProperties.ValueRW.centerEyePosition = (float3)position;
            }
        }

        #endregion
    }
}