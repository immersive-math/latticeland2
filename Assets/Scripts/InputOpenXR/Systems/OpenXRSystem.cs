using InputOpenXR.Aspects;
using Unity.Burst;
using Unity.Entities;

namespace InputOpenXR.Systems
{
    public partial struct OpenXRSystem : ISystem
    {
        [BurstCompile]
        public void OnCreate(ref SystemState state)
        {
        }

        [BurstCompile]
        public void OnDestroy(ref SystemState state)
        {
        }

        [BurstCompile]
        public void OnUpdate(ref SystemState state)
        {
            foreach (var controller in SystemAPI.Query<XRControllerAspect>())
            {
                controller.UpdateOpenXRPosition();
                controller.UpdateOpenXRInput();
            }

            foreach (var hmd in SystemAPI.Query<XRHMDAspect>())
            {
                hmd.UpdateOpenXRPosition();
                hmd.UpdateOpenXRcenterEye();
            }
        }
    }
}