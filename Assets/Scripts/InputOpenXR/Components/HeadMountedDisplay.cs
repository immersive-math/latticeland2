using Unity.Entities;
using Unity.Mathematics;
using UnityEngine;

namespace InputOpenXR.Components
{
    public struct HeadMountedDisplayComponent : IComponentData
    {
        public bool isTracked;
        public float3 centerEyePosition;
    }

    public class HeadMountedDisplay : MonoBehaviour
    {
        [Header("Logic Variables")]

        #region Logic Variables

        public float3 centerEyePosition;

        public bool isTracked;

        #endregion
    }

    public class HeadMountedDisplayBaker : Baker<HeadMountedDisplay>
    {
        public override void Bake(HeadMountedDisplay authoring)
        {
            AddComponent(new HeadMountedDisplayComponent
            {
                isTracked = authoring.isTracked,
                centerEyePosition = authoring.centerEyePosition,
            });
        }
    }
}