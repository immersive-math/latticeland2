﻿using Unity.Entities;
using Unity.Mathematics;
using UnityEngine;

namespace InputOpenXR.Components
{
    public struct ControllerComponent : IComponentData
    {
        public bool isTracked;

        public float radius;
        public float3 transPos;


        #region Handedness

        public bool isLeft;

        #endregion

        //map OpenXR interactions; TODO: could this be refactored into multiple component data?

        #region Input Variables

        #region Trigger

        public float triggerValue;
        public bool triggerPressed;
        public bool triggerTouched;

        #endregion

        #region Squeeze

        public float squeezeValue;
        public bool squeezeClick;
        public bool squeezeForce;

        #endregion

        #region Thumbstick

        public float2 thumbstick;
        public float2 thumbstickTouch;
        public float2 thumbstickClick;

        #endregion

        #region Trackpad

        public float2 trackpad;
        public bool trackpadTouch;
        public bool trackpadClick;

        #endregion

        #region Buttons

        public bool primaryClick; //maps to A or X
        public bool primaryTouch; //maps to A or X
        public bool secondaryClick; //maps to B or Y
        public bool secondaryTouch; //maps to B or Y
        public bool systemClick;
        public bool systemTouch;
        public bool selectClick;
        public bool menuClick;

        #endregion

        #endregion
    }

    public class Controller : MonoBehaviour
    {
        [Header("Logic Variables")]

        #region Logic Variables

        public bool isTracked;

        public float radius;

        #endregion

        #region Handedness

        public bool isLeft;

        #endregion

        //map OpenXR interactions; TODO: could this be refactored into multiple component data?

        [Header("Input Variables")]

        #region Input Variables

        #region Trigger

        public float triggerValue;

        public bool triggerPressed;
        public bool triggerTouched;

        #endregion

        #region Squeeze

        public float squeezeValue;
        public bool squeezeClick;
        public bool squeezeForce;

        #endregion

        #region Thumbstick

        public float2 thumbstick;
        public float2 thumbstickTouch;
        public float2 thumbstickClick;

        #endregion

        #region Trackpad

        public float2 trackpad;
        public bool trackpadTouch;
        public bool trackpadClick;

        #endregion

        #region Buttons

        public bool primaryClick; //maps to A or X
        public bool primaryTouch; //maps to A or X
        public bool secondaryClick; //maps to B or Y
        public bool secondaryTouch; //maps to B or Y
        public bool systemClick;
        public bool systemTouch;
        public bool selectClick;
        public bool menuClick;

        #endregion

        #endregion
    }

    public class ControllerBaker : Baker<Controller>
    {
        public override void Bake(Controller authoring)
        {
            AddComponent(new ControllerComponent
            {
                isTracked = authoring.isTracked,
                radius = authoring.radius,
                isLeft = authoring.isLeft,
                squeezeValue = authoring.squeezeValue,
                squeezeClick = authoring.squeezeClick,
                squeezeForce = authoring.squeezeForce,
                triggerValue = authoring.triggerValue,
                triggerPressed = authoring.triggerPressed,
                triggerTouched = authoring.triggerTouched,
                thumbstick = authoring.thumbstick,
                thumbstickClick = authoring.thumbstickClick,
                thumbstickTouch = authoring.thumbstickTouch,
                trackpad = authoring.trackpad,
                trackpadTouch = authoring.trackpadTouch,
                trackpadClick = authoring.trackpadClick,
                primaryClick = authoring.primaryClick,
                primaryTouch = authoring.primaryTouch,
                secondaryClick = authoring.secondaryClick,
                secondaryTouch = authoring.secondaryTouch,
                systemClick = authoring.systemClick,
                systemTouch = authoring.systemTouch,
                selectClick = authoring.systemClick,
                menuClick = authoring.menuClick
            });
        }
    }
}